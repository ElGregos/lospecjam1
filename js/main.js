"use strict"

//Main module.
import App from './modules/app.module.js'

let _app = new App()
_app.init()
