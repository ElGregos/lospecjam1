let setup = {
    canvas: document.getElementById('canvas'),
    zoomMax: 6,
    screen: { width: 240, height: 135 },
    colors: ['#eae1f0', '#7e7185', '#37313b', '#1d1c1f', '#89423f', '#f63f4c', '#fdbb27', '#8d902e', '#4159cb', '#59a7af'],
    boot: {
        colors: {
            border: 8, bg: 8, fg: 6,
            // border: 3, bg: 3, fg: 5,
            // border: 5, bg: 0, fg: 3,
        }
    },
    controls: {
        left: { codes: ['ArrowLeft', 'KeyA'] },
        right: { codes: ['ArrowRight', 'KeyD'] },
        up: { codes: ['ArrowUp', 'KeyW'] },
        down: { codes: ['ArrowDown', 'KeyS'] },
        btn1: { codes: ['Space', 'KeyZ', 'KeyC', 'KeyN'] },
        btn2: { codes: ['ControlLeft', 'KeyX', 'KeyV', 'KeyM'] },
        btn3: { codes: ['Enter', 'Escape', 'KeyP'] },
    },
    fonts: [
        {
            name: 'font3x6',
            file: './img/font3x6.png',
            width: 3, height: 6,
            chars: '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~',
        }
    ],
    tilesets: [
        {
            name: 'normal',
            file: './img/sprites.png',
            sprite: { width: 12, height: 12 },
            autotile: ''
        },
        {
            name: 'greyed',
            file: './img/spritesGreyed.png',
            sprite: { width: 12, height: 12 }
        }
    ],
    autotile: true
}