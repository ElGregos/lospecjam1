/**
 * Handles controls & keys (keyboard).
 * Control != Key!!
 * Control = input triggering an action, or the action itself.
 * Key = physical key activating a control.
 * A control may have several keys to activate it.
 * A key activates only one control.
 *
 * BEWARE: DON'T CHECK TWICE A CONTROL!
 * It would be buggy because of pause & repeat on keys.
 * Instead put the test in a variable you'll check.
 * Testing twice a control, esp. with different pause & repeat, doesn't make sense anyway.
 * TODO: cache each control down state on each frame so that multiple calls won't bring issues.
 */

export default class Controls {
    //PROPERTIES
    app = null
    keyCtrls = {}

    //METHODS

    constructor() {
        document.addEventListener('keydown', (event) => {
            let key = event.code
            if (key === 'F5') event.preventDefault()
            if (this.keyCtrls[key]) {
                if (!this.keyCtrls[key].isDown) {
                    this.keyCtrls[key].isDown = true
                    this.keyCtrls[key].ctrls.forEach(ctrl => {
                        setup.controls[ctrl].nbDown++
                    })
                }
            }
        })
        document.addEventListener('keyup', (event) => {
            // console.log(event)
            let key = event.code
            if (this.keyCtrls[key]) {
                this.keyCtrls[key].isDown = false
                this.keyCtrls[key].ctrls.forEach(ctrl => {
                    setup.controls[ctrl].nbDown = Math.max(setup.controls[ctrl].nbDown - 1, 0)
                })
                let ctrl = this.keyCtrls[key].ctrls[0]
                setup.controls[ctrl].pressedSince = 0
            }
        })
    }

    init = app => {
        this.app = app
        this.set()
    }

    set = () => {
        /**
         * Create this.keyCtrls, which is a rotation of setup,
         * ie setup = each control has its keys,
         * whereas keyCode = each key has its controls
         */
        Object.keys(setup.controls).forEach(ctrl => {
            setup.controls[ctrl].isDown = false
            setup.controls[ctrl].nbDown = 0
            setup.controls[ctrl].pressedSince = 0
            setup.controls[ctrl].codes.forEach(code => {
                this.keyCtrls[code] = this.keyCtrls[code] || { isDown: false, ctrls: [] }
                this.keyCtrls[code].ctrls[0] = ctrl
            })
        })
    }

    //State checkers.
    /**
     *
     * @param {String} ctrl - Control to check
     * @param {Number} pause - n/60s before repeating. -1 = never.
     * @param {Number} repeat - n/60s between each retrigger.
     * @returns
     */
    isDown = (ctrl, pause, repeat) => {
        pause = pause || 0
        repeat = repeat || 1
        let pressedSince = setup.controls[ctrl].pressedSince
        //A ctrl is down if one of its key is down and
        //- either the ctrl was not pressed the frame before
        //- or it's beyond its pause and at a repeat.
        let isDown = (setup.controls[ctrl].nbDown > 0) && (pressedSince === 0 || (pause >= 0 && pressedSince > pause && mod(pressedSince - pause, repeat) < 1))
        if (setup.controls[ctrl].nbDown > 0) setup.controls[ctrl].pressedSince++
        return isDown
    }

    isDownOnce = ctrl => {
        let state = this.isDown(ctrl, -1)
        return state
    }

    isDownP = ctrl => {
        let state = this.isDown(ctrl, 6, 4)
        return state
    }

    isUp = ctrl => {
        let state = setup.controls[ctrl].nbDown <= 0
        return state
    }

}