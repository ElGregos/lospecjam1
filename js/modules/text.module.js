/**
 * Handles bitmap texts and fonts.
 */

export default class Text {
    //PROPERTIES
    ctx = setup.canvas.getContext('2d')
    currentColor = 0
    currentFont
    currentX = 0
    currentY = 0
    fonts = {}

    constructor() {
        this.init(setup.fonts)
    }

    //METHODS

    /**
     * Set color or get current color.
     * @param {Number} c
     * @returns Number
     */
    color = c => {
        c = (typeof c === 'number') ? c : this.currentColor
        this.currentColor = c
        return this.currentColor
    }

    cursor = (x, y) => {
        this.currentX = x
        this.currentY = y
    }

    //Adds font to this.fonts.
    fontAdd = font => {
        this.fonts[font.name] = {
            file: font.file,
            image: this.imageLoad(font.file),
            chars: font.chars,
            width: font.width, height: font.height
        }
        this.currentFont = this.currentFont || this.fonts[font.name]
    }

    //Loads & returns image.
    imageLoad = file => {
        let image = new Image()
        image.src = file
        return image
    }

    init = fonts => {
        fonts.forEach(font => {
            this.fontAdd(font)
        })
    }

    //Prints str at x,y
    print = (str, x, y, c, zoomX, zoomY) => {
        let xo = x = (typeof x === 'number') ? x : this.currentX
        let yo = y = (typeof y === 'number') ? y : this.currentY
        c = this.color(c)
        zoomX = zoomX || 1
        zoomY = zoomY || 1
        str = str.toString()
        for (let charN = 0; charN < str.length; charN++) {
            let strChar = str.charAt(charN),
                charInd = this.currentFont.chars.indexOf(strChar)
            if (charInd >= 0) {
                this.ctx.drawImage(
                    this.currentFont.image,
                    charInd * (this.currentFont.width + 1), c * (this.currentFont.height + 1),
                    this.currentFont.width, this.currentFont.height,
                    x, y,
                    this.currentFont.width * zoomX, this.currentFont.height * zoomY
                )
            }
            x += (this.currentFont.width + 1) * zoomX
        }
        this.cursor(xo, yo + 7)
    }

    type = (text, wait, startTime) => {
        startTime = (typeof startTime === 'number') ? startTime : 0
        return text
    }

}