export default class Sprites {
    //PROPERTIES
    ctx = setup.canvas.getContext('2d')
    tilesets = {}
    tileset //Current tileset

    constructor() {
        this.init()
    }

    //METHODS
    draw = (spr, x, y) => {
        let spx = spr % 16 * this.tileset.sprite.width,
            spy = Math.floor(spr / 16) * this.tileset.sprite.height
        this.ctx.drawImage(
            this.tileset.image,
            spx, spy,
            this.tileset.sprite.width, this.tileset.sprite.height,
            x, y,
            this.tileset.sprite.width, this.tileset.sprite.height
        )
    }

    //Loads & returns image.
    imageLoad = file => {
        let image = new Image()
        image.src = file
        return image
    }

    init = () => {
        this.tilesetsInit(setup.tilesets)
    }

    /**
     * Change current tileset.
     * @param {String} name
     */
    setTileset = name => {
        this.tileset = this.tilesets[name]
    }

    tilesetsInit = tilesets => {
        tilesets.forEach(tileset => {
            tileset.image = this.imageLoad(tileset.file)
            this.tilesets[tileset.name] = tileset
        })
        //Set current tileset on 1st one.
        this.setTileset(Object.keys(this.tilesets)[0])
    }
}