import Controls from './controls.module.js'
const _controls = new Controls()
import Menu from './menu.module.js'
const _menu = new Menu()
import View from './view.module.js'
const _view = new View()

export default class App {

    //PROPERTIES
    controlsModule = _controls
    menuModule = _menu
    viewModule = _view

    cell = new Cell()
    level = new Level()
    log = new Log()
    player = new Player()

    menus = {
        reboot: {
            options: [
                {
                    label: 'REBOOT> Are you sure?'
                },
                _menu.optionNo, {
                    label: 'Yes', action: () => {
                        this.reboot()
                    }
                }
            ],
            onClose: () => { this.setState('main') }
        },
        pause: {
            options: [
                _menu.optionClose,
                {
                    label: 'Re-login',
                    action: () => {
                        this.playerReconnect()
                        _menu.close()
                    }
                },
                {
                    label: () => {
                        // return null
                        if (this.level.currentN === 0) {
                            return null
                            return 'Connect to previous area'
                        } else {
                            return 'Connect to my area'
                        }
                    },
                    action: () => {
                        this.levelGoTo(0)
                        _menu.close()
                    }
                },
                {
                    label: 'Reboot',
                    action: () => {
                        // _menu.close()
                        _menu.open(this.menus.reboot)
                    }
                },
            ],
            onClose: () => { this.setState('main') }
        }
    }

    states = {
        launch: {
            update: () => this.updateIntro(),
            draw: () => _view.drawIntro(this)
        },
        main: {
            update: () => this.updateGame(),
            draw: () => _view.drawGame(this)
        },
        mainPaused: {
            init: () => {
                _menu.open(this.menus.pause)
            },
        },
    }

    state = {}
    time = 0
    timeMs = 0

    //METHODS
    getTimer = () => {
        return this.time - this.startTime
    }

    init = () => {
        this.initModules()
        this.initGame()
        window.requestAnimationFrame(this.loop)
    }

    initGame = () => {
        this.cell.initList()
        this.level.initList()
        this.log.init()
        this.levelGoTo(0)
        // this.setState('launch')
        this.setState('main')
    }

    initModules = () => {
        _controls.init(this)
        _menu.init(this)
        _view.init(this)
    }

    levelGoTo = levelN => {
        if (levelN < this.level.list.length) {
            this.level.init(levelN)
            this.playerReconnect()
            // this.player.placeOnLevel(this.level)
        }
    }

    levelGoToNext = () => {
        this.log.add(`Connecting to area ${this.level.currentN + 1}`)
        if (this.level.currentN < this.level.list.length - 1) {
            this.levelGoTo(this.level.currentN + 1)
        } else {
            //Current level is the last one.
            console.log("Thanks for playing!")
        }
    }

    loop = (timeMs) => {
        window.requestAnimationFrame(this.loop)
        let time = timeMs / (1000 / 60)
        this.delay = time - this.time
        this.time = time
        this.timeMs = timeMs
        this.update()
        _view.draw(this)
    }

    playerReconnect = () => {
        this.log.add("User connected")
        this.level.scroll = this.level.originalScroll
        this.player.placeOnLevel(this.level)
    }

    reboot = () => {
        this.initGame()
        this.setState('launch')
    }

    setState = state => {
        this.startTime = this.time
        this.state = this.states[state]
        if (this.state.init) this.state.init()
        if (this.state.update) this.update = this.state.update
        if (this.state.draw) this.draw = this.state.draw
    }

    updateGame = () => {
        let sx = 0,
            sy = 0,
            //Keep track of level movement.
            lvlxo = this.level.xo,
            lvlyo = this.level.yo
        if (_controls.isDownP('left')) {
            sx--
        }
        if (_controls.isDownP('right')) {
            sx++
        }
        if (_controls.isDownP('up')) {
            sy--
        }
        if (_controls.isDownP('down')) {
            sy++
        }
        //Restart player blink?
        if (sx !== 0 || sy !== 0) {
            this.player.blink = 0
        }

        //No diagonal move in a frame (yes this happens).
        if (sx !== 0) sy = 0

        //Scrolling?
        let spriteAhead
        if (this.level.scroll) {
            //Hit wall?
            let spriteX = this.player.isBlocked(this.level, this.player.x + sx, this.player.y)
            let spriteY = this.player.isBlocked(this.level, this.player.x, this.player.y + sy)
            spriteAhead = spriteX || spriteY
            sx = spriteX ? 0 : sx
            sy = spriteY ? 0 : sy
            //Hit border?
            if (this.player.x + sx < this.level.windowLeft) {
                sx = 0
                this.level.xo--
            }
            if (this.player.x + sx > this.level.windowRight) {
                sx = 0
                this.level.xo++
            }
            if (this.player.y + sy < this.level.windowTop) {
                sy = 0
                this.level.yo--
            }
            if (this.player.y + sy > this.level.windowBottom) {
                sy = 0
                this.level.yo++
            }
        } else {
            //Looping. Hit border?
            if (this.player.x + sx < this.level.windowLeft || this.player.x + sx > this.level.windowRight) {
                sx *= -this.level.window.width + 2 * this.level.window.border + 1
            }
            if (this.player.y + sy < this.level.windowTop || this.player.y + sy > this.level.windowBottom) {
                sy *= -this.level.window.height + 2 * this.level.window.border + 1
            }
            //Hit wall?
            let spriteX = this.player.isBlocked(this.level, this.player.x + sx, this.player.y)
            let spriteY = this.player.isBlocked(this.level, this.player.x, this.player.y + sy)
            spriteAhead = spriteX || spriteY
            sx = spriteX ? 0 : sx
            sy = spriteY ? 0 : sy
        }
        if (spriteAhead === 52) this.log.add("Access denied")
        this.player.x += sx
        this.player.y += sy
        this.player.x = mod(this.player.x, this.level.window.width)
        this.player.y = mod(this.player.y, this.level.window.height)

        let playerMoved = sx != 0 || sy != 0 || lvlxo != this.level.xo || lvlyo != this.level.yo
        if (!playerMoved) {
            if (_controls.isDownOnce('btn1')) {
                let spr = this.level.getSprite(this.player.x, this.player.y)
                let cell = this.cell.list[spr]
                if (cell.action && typeof cell.action.callback === 'function') {
                    cell.action.callback(this)
                }
            }
            if (_controls.isDownOnce('btn3')) {
                this.setState('mainPaused')
            }
        }
        this.player.blink += this.delay
        this.player.blink %= 60
    }

    updateIntro = () => {
        if (_controls.isDownOnce('btn3')) {
            this.setState('main')
        }
    }

}
