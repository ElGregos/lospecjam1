import Text from './text.module.js'
const _text = new Text()
import View from './view.module.js'
const _view = new View()

export default class Menu {
    //PROPERTIES
    app
    currentOptionN = 0
    enabledIndexes = []
    height = 0
    onClose = () => { }
    optionClose = {
        label: 'Close',
        action: () => { this.close() }
    }
    optionNo = {
        label: 'No',
        action: () => { this.close() }
    }
    options = []
    callerDraw
    width = 0
    xo = 0
    yo = 0

    //GETTERS
    get currentOption() { return this.options[this.enabledIndexes[this.currentOptionN]] }

    //METHODS
    close = () => {
        this.app.draw = this.callerDraw
        this.callerDraw = null
        this.onClose()
    }

    draw = () => {
        this.callerDraw()
        _view.darkenBg()
        _view.rectfill(this.xo - 1, this.yo - 1, this.width + 2, this.height + 2, 3)
        _view.rect(this.xo, this.yo, this.width, this.height, 6)
        let index = 0
        this.options.forEach((option) => {
            let c = 1,
                label = this.getOptionLabel(option)
            if (label) {
                if (this.currentOption === option) {
                    c = 0
                    if (option.set) {
                        label = `<${label}>`
                    } else {
                        label = `>${label}`
                    }
                } else {
                    label = ` ${label}`
                }
                _text.print(label, this.xo + 6, this.yo + 6 + index * 8, c)
                index++
            }
        })
    }

    getLongestLabelLength = () => {
        let len = 0
        this.options.forEach((option) => {
            let label = this.getOptionLabel(option)
            if (label) {
                len = Math.max(len, label.length * (_text.currentFont.width + 1) + 19)
            }
        })
        return len
    }

    getOptionsCount = () => {
        let count = 0
        this.options.forEach((option) => {
            if (this.getOptionLabel(option)) {
                count++
            }
        })
        return count
    }

    getOptionLabel = option => {
        let label = option.label
        if (typeof label === 'function') {
            label = label()
        }
        return label
    }

    init = app => {
        this.app = app
    }

    open = menu => {
        this.options = menu.options
        this.onClose = menu.onClose
        this.setEnabledIndexes()
        this.currentOptionN = 0

        //Find menu width & height, xo & yo.
        this.width = Math.max(50, this.getLongestLabelLength(), menu.width || 0)
        this.height = this.getOptionsCount() * 8 + 9
        this.xo = Math.floor((setup.screen.width - this.width) / 2)
        this.yo = Math.floor((setup.screen.height - this.height) / 2)

        //Set callerDraw if no menu was opened just before.
        if (!this.callerDraw) {
            this.callerDraw = this.app.draw
        }

        this.app.draw = this.draw
        this.app.update = this.update
        return this
    }

    /**
     * Set indexes from enabled options: those with an action property.
     */
    setEnabledIndexes = () => {
        this.enabledIndexes = []
        this.options.forEach((option, index) => {
            if (typeof option.action === 'function'
                && this.getOptionLabel(option)
            ) {
                this.enabledIndexes.push(index)
            }
        })
    }

    update = () => {
        if (this.app.controlsModule.isDownOnce('up')) {
            this.currentOptionN--
        }
        if (this.app.controlsModule.isDownOnce('down')) {
            this.currentOptionN++
        }
        this.currentOptionN = mod(this.currentOptionN, this.enabledIndexes.length)

        //Option in mode Set?
        if (this.currentOption.set) {
            //Mode Set: change value.
            if (this.currentOption.set.range) {
                if (this.app.controlsModule.isDownP('left')) {
                    this.currentOption.set.value -= this.currentOption.set.range.step
                }
                if (this.app.controlsModule.isDownP('right')) {
                    this.currentOption.set.value += this.currentOption.set.range.step
                }
                this.currentOption.set.value = mid(this.currentOption.set.range.min, this.currentOption.set.value, this.currentOption.set.range.max)
            }
        }
        //Validate option.
        if (this.app.controlsModule.isDownOnce('btn1')
            || this.app.controlsModule.isDownOnce('btn3')
            || this.currentOption.set
        ) {
            this.currentOption.action(this)
        }
        if (this.app.controlsModule.isDownOnce('btn2')) {
            this.close()
        }
    }
}