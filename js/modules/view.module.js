import Sprites from './sprites.module.js'
const _sprites = new Sprites()
import Text from './text.module.js'
const _text = new Text()

export default class View {

    //PROPERTIES
    app
    currentColor = 0
    canvas = setup.canvas
    ctx = this.canvas.getContext('2d')

    //METHODS
    color = c => {
        c = (typeof c === 'number') ? c : this.currentColor
        this.ctx.strokeStyle = setup.colors[Math.floor(c)]
        this.ctx.fillStyle = setup.colors[Math.floor(c)]
        this.currentColor = c
    }

    cls = c => {
        this.color(c)
        this.ctx.fillRect(0, 0, setup.screen.width, setup.screen.height)
    }

    darkenBg = () => {
        // console.log(this)
        for (let y = 0; y <= 135; y += 2) {
            this.lineH(0, 240, y, 3)
        }
    }

    draw = () => {
        this.ctx.imageSmoothingEnabled = false
        _text.cursor(0, 0)
        this.app.draw()
    }

    drawGame = () => {
        this.cls(2)
        // let xo = 112,
        let xo = 7,
            yo = 7
        this.drawGameGrid(xo, yo)
        this.drawGamePanel(136, yo - 3)
        //Player
        this.rect(xo + this.app.player.x * 12, yo + this.app.player.y * 12, 13, 13, this.app.player.color)
    }

    drawGameGrid = (xo, yo) => {
        //Frame
        this.rectfill(xo - 3, yo - 3, this.app.level.window.width * 12 + 7, this.app.level.window.height * 12 + 7, 3)
        this.rect(xo, yo, this.app.level.window.width * 12 + 1, this.app.level.window.height * 12 + 1, this.app.level.scroll ? 8 : 2)

        //Map
        for (let cy = 0; cy < this.app.level.window.height; cy++) {
            for (let cx = 0; cx < this.app.level.window.width; cx++) {
                let px = cx * 12 + xo,
                    py = cy * 12 + yo,
                    spr = this.app.level.getSprite(cx, cy, setup.autotile)
                //If sprite's outside the window in loop mode, grey sprite out.
                if (!this.app.level.scroll && (cx < this.app.level.windowLeft || cx > this.app.level.windowRight || cy < this.app.level.windowTop || cy > this.app.level.windowBottom)
                ) {
                    _sprites.setTileset('greyed')
                }
                _sprites.draw(spr, px, py)
                _sprites.setTileset('normal')
            }
        }

        //Window border
        let bx = xo + this.app.level.windowLeft * 12,
            by = yo + this.app.level.windowTop * 12,
            bw = (this.app.level.window.width - 2 * this.app.level.window.border) * 12 + 1,
            bh = (this.app.level.window.height - 2 * this.app.level.window.border) * 12 + 1
        this.rect(bx - 1, by - 1, bw + 2, bh + 2, 3)
        this.rect(bx + 1, by + 1, bw - 2, bh - 2, 3)
        if (this.app.level.scroll) {
            this.rect(bx, by, bw, bh, 9)
        } else {
            this.rect(bx, by, bw, bh, 8)
        }
    }

    drawGamePanel = (xo, yo) => {
        let w = 101
        _text.print('MemoReader', xo, yo, 6, 2, 2)
        _text.print('v1.0', xo + 85, yo + 5)

        //Draw cell.
        yo += 16
        let spr = this.app.level.getSprite(this.app.player.x, this.app.player.y),
            cell = this.app.cell.list[spr]
        this.rectfill(xo - 1, yo, w, 15, 3)
        _sprites.draw(spr, xo, yo + 1)
        this.rect(xo, yo + 1, 13, 13, 8)
        let info = cell ? cell.info : 'No data found'
        _text.print(info.toUpperCase(), xo + 14, yo + 1, 0)
        if (cell && cell.action) {
            _text.print(`[SPACE] ${cell.action.label}`, xo + 14, yo + 8, 6)
        }

        yo += 28
        this.drawLogs(xo, yo)

        yo += 78
        _text.print('[ENTER] Menu', xo, yo, 6)
    }

    drawIntro = () => {
        let lines = [
            { wait: 0, text: _text.type(' LOSPEC 128K computer terminal (v1)', 20) },
            { wait: 10, text: ' 10 colors 240x135 dots graphics' },
            { wait: 10, text: ` ${new Date('1982-07-09').toLocaleDateString()}, ${new Date().toLocaleTimeString()}` },
            { wait: 0, text: '' },
            { wait: 30, text: 'Ready' },
            { wait: 40, text: _text.type('load "memoread",2', 10) },
            { wait: 40, text: '"memoread" loaded from mainframe computer' },
            { wait: 40, text: _text.type('run', 10) },
            { wait: 0, text: '' },
            { wait: 60, text: 'Welcome to MemoRead v1.0' },
            { wait: 0, text: 'Press [ENTER] to start' },
        ],
            wait = 0
        this.cls(setup.boot.colors.border)
        this.rectfill(10, 10, 220, 115, setup.boot.colors.bg)
        _text.color(setup.boot.colors.fg)
        _text.cursor(10, 10)
        lines.forEach((line) => {
            wait += line.wait
            if (this.app.getTimer() > wait) {
                _text.print(line.text)
            }
        })
    }

    drawLogs = (xo, yo) => {
        _text.print(`[CTRL] Logs (${this.app.log.list.length})`, xo, yo - 7, 6)
        this.rectfill(xo - 1, yo, 101, 71, 3)
        let logs = this.app.log.list.slice(Math.max(0, this.app.log.list.length - 10))
        // console.log(logs.length)
        for (let logN = 0; logN < logs.length; logN++) {
            let message = logs[logN]
            _text.print(message, xo, yo + logN * 7 + 1, 1)
        }
    }

    init = app => {
        this.app = app
        this.resize()
        window.addEventListener('resize', this.resize)
    }

    lineH = (x1, x2, y, c) => {
        this.color(c)
        this.ctx.beginPath()
        this.ctx.moveTo(x1, y + .5)
        this.ctx.lineTo(x2, y + .5)
        this.ctx.stroke()
    }

    lineV = (x, y1, y2, c) => {
        this.color(c)
        this.ctx.beginPath()
        this.ctx.moveTo(x + .5, y1)
        this.ctx.lineTo(x + .5, y2)
        this.ctx.stroke()
    }

    pset = (x, y, c) => {
        this.color(c)
        this.ctx.fillRect(x, y, 1, 1)
    }

    rect = (x, y, w, h, c) => {
        this.color(c)
        this.ctx.strokeRect(x + .5, y + .5, w - 1, h - 1)
    }

    rectfill = (x, y, w, h, c) => {
        this.color(c)
        this.ctx.fillRect(x, y, w, h)
    }

    resize = () => {
        this.canvas.style.marginTop = 0//Stick to top to prevent scrollbar to mess with screen width.
        this.canvas.width = setup.screen.width
        this.canvas.height = setup.screen.height
        //Resize to container with an integer ratio.
        let hRatio = this.canvas.parentElement.clientWidth / this.canvas.width,
            vRatio = this.canvas.parentElement.clientHeight / this.canvas.height
        this.zoom = Math.max(1, Math.floor(Math.min(hRatio, vRatio, setup.zoomMax)))
        this.canvas.style.width = setup.screen.width * this.zoom
        this.canvas.style.height = setup.screen.height * this.zoom
        this.canvas.style.marginTop = (this.canvas.parentElement.clientHeight - setup.screen.height * this.zoom) / 2
    }

}