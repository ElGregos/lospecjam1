"use strict"

//Returns middle of 3 values.
const mid = (a, b, c) => {
    if (a < b) {
        return (b < c) ? b : ((a < c) ? c : a)
    } else {
        return (a < c) ? a : ((b < c) ? c : b)
    }
}

//Modulo (% is a remainder, unusable with negative values).
const mod = (a, n) => {
    let m = ((a % n) + n) % n
    return m
    // return m < 0 ? m + Math.abs(n) : m
}
