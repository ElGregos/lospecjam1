class Level {
    //PROPERTIES
    currentN = 0
    list = []
    map = []
    scroll = false
    window = {
        width: 0,
        height: 0,
        border: 0
    }
    xo = 0
    yo = 0

    //GETTERS
    get height() { return this.map.length }
    get width() { return this.map[0].length / 2 }
    get windowLeft() { return this.window.border }
    get windowRight() { return this.window.width - this.window.border - 1 }
    get windowTop() { return this.window.border }
    get windowBottom() { return this.window.height - this.window.border - 1 }

    //METHODS
    getSprite = (ocx, ocy, autotile) => {
        let cx = mod(ocx + this.xo, this.width)
        let cy = mod(ocy + this.yo, this.height)
        let spr = parseInt(this.map[cy].substring(cx * 2, cx * 2 + 2), 16)
        if (spr === 3 && autotile) {
            let sps = [3, 2, 19, 18, 0, 1, 16, 17, 51, 50, 35, 34, 48, 49, 32, 33]
            let neighs = [-1, 0, 0, 1, 1, 0, 0, -1]
            let tot = 0
            for (let n = 0; n <= 6; n += 2) {
                let sp = this.getSprite(ocx + neighs[n], ocy + neighs[n + 1], false)
                if (sp === 3) tot += 2 ** (n / 2)
            }
            spr = sps[tot]
        } else if (spr === 36 || spr === 37) {
            spr = this.scroll ? 36 : 37
        }
        return spr
    }

    getRawSprite = (cx, cy) => parseInt(this.map[cy].substring(cx * 2, cx * 2 + 2), 16)

    init = levelN => {
        this.currentN = (typeof levelN === 'undefined' ? this.currentN : levelN)
        this.map = this.list[this.currentN].map
        this.plyr = this.list[this.currentN].plyr || {
            xo: 0, yo: 0
        }
        this.scroll = this.list[this.currentN].scroll
        this.originalScroll = this.scroll
        this.window = {
            width: 10,
            height: 10,
            border: 2
        }
        this.xo = 0
        this.yo = 0
    }

    initList = () => {
        this.list = [
            {
                map: [
                    '040714141414',
                    '141414141414',
                    '141414141414',
                    '141414141414',
                    '141414141414',
                    '141414141426',
                ],
                scroll: false
            },
            {
                map: [
                    '14140314',
                    '14040314',
                    '03030303',
                    '14140326',
                ],
                plyr: { xo: 1, yo: 1 },
                scroll: false
            },
            {
                map: [
                    '140303030303140303030303',
                    '030314032403140326030603',
                    '241414031403030314031403',
                    '140303031414141414031403',
                    '140314030303140303033403',
                    '140314140414141414141403',
                    '140314030303030303030303',
                    '140314031414241414140314',
                    '140303030303031415140314',
                    '141414141414031414140324',
                    '030303030303030314030303',
                    '141414141414141414141414',
                ],
                plyr: { xo: 2, yo: 3 },
                scroll: true
            },
        ]
    }

}
