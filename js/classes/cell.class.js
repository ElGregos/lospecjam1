class Cell {
    list = []

    initList = () => {
        this.list[4] = { info: "login address" }
        this.list[7] = {
            info: "configuration program",
            action: {
                label: 'Run',
                callback: app => {
                    app.menuModule.open({
                        options: [
                            { label: 'Graphics' },
                            {
                                label: () => `Max zoom: ${setup.zoomMax}`,
                                set: {
                                    range: { min: 1, max: 15, step: 1 },
                                    value: setup.zoomMax,
                                },
                                action: menu => {
                                    setup.zoomMax = menu.currentOption.set.value
                                    app.viewModule.resize()
                                }
                            },
                            app.menuModule.optionClose,
                        ],
                        onClose: () => { app.setState('main') }
                    })
                }
            }
        }
        this.list[20] = { info: "empty cell" }
        // this.list[21] = { info: "program", action: "run" }
        this.list[22] = { info: "raw data", action: "Read" }
        this.list[36] = {
            info: "scroll>loop",
            action: {
                label: "Run",
                callback: app => app.level.scroll = !app.level.scroll
            }
        }
        this.list[37] = {
            info: "loop>scroll",
            action: {
                label: "Run",
                callback: app => app.level.scroll = !app.level.scroll
            }
        }
        this.list[38] = {
            info: "Link to next area",
            action: {
                label: "Connect",
                callback: app => {
                    app.levelGoToNext()
                }
            }
        }
        this.list[39] = { info: "lvl 1 password", action: "Download" }
        this.list[52] = { info: "lvl 1 lock", action: "Unlock" }
    }

}
