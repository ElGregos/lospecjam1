class Player {
    //PROPERTIES
    x = 0
    y = 0
    blink = 0

    //GETTERS
    get color() { return (this.blink % 60 < 30) ? 0 : 9 }

    //METHODS
    /**
     * Returns the sprite at cx,cy if it's blocking player, null otherwise.
     * @param {Level} level
     * @param {Number} cx
     * @param {Number} cy
     * @returns Number||null
     */
    isBlocked = (level, cx, cy) => {
        let spr = level.getSprite(cx, cy),
            isBlocked
        if (spr === 3
            || spr === 52
        ) {
            isBlocked = spr
        }
        return isBlocked
    }

    //Place player on start position.
    placeOnLevel = level => {
        loop:
        for (let cy = 0; cy < level.height; cy++) {
            for (let cx = 0; cx < level.width; cx++) {
                let spr = level.getRawSprite(cx, cy)
                if (spr === 4) {
                    this.x = level.window.border + level.plyr.xo
                    this.y = level.window.border + level.plyr.yo
                    level.xo = -level.window.border + cx - level.plyr.xo
                    level.yo = -level.window.border + cy - level.plyr.yo
                    break loop
                }
            }
        }
    }

}
